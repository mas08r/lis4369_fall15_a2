> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369-Extensible Enterprise Solutions Fall15 A2:

## Mark Sterrenberg:

### Assignment Requirements:

#### A2 Assignment Fully Functional
![A2 Working](https://bitbucket.org/mas08r/lis4369_fall15_a2/raw/master/images/working.jpg "A2 Working")
#### A2 Assignment Error
![A2 Error](https://bitbucket.org/mas08r/lis4369_fall15_a2/raw/master/images/error.jpg "A2 Error")
#### A2 Assignment Extra Credit
![A2 Extra Credit](https://bitbucket.org/mas08r/lis4369_fall15_a2/raw/master/images/extra_credit.jpg "A2 Extra Credit")

